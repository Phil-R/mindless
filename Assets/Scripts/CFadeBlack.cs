﻿using UnityEngine;
using System.Collections;

public class CFadeBlack : MonoBehaviour {

	public Material black;

	public float alpha = 1.0f;
	public bool fadeToBlack = false;

	// Use this for initialization
	void Start () {
	

	}
	
	// Update is called once per frame
	void Update () {
	
		if (fadeToBlack) 
		{
			if(alpha < 1.0f)
				alpha += 0.05f;
		} 
		else 
		{
			if(alpha > 0.0f)
				alpha -= 0.05f;
		}

		black.color = new Color (0.0f, 0.0f, 0.0f, alpha);

	}
}
