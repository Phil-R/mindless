﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CRevealMap : MonoBehaviour {

	public GameObject mapPiece;
	private Image mapImage;

	void Start(){

		mapImage = mapPiece.GetComponent<Image> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			mapPiece.SetActive (true);
			mapImage.color = Color.cyan;
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player") 
		{
			mapImage.color = Color.white;
		}
	}

}
