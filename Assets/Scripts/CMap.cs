﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CMap : MonoBehaviour {

	public GameObject map;
	public GameObject mapUG;
	public GameObject mapEG;
	public GameObject mapOG;

	public Text UGText;
	public Text EGText;
	public Text OGText;

	public bool mapOpen = false;
	public int displayedFloor = 1;

	public AudioClip mapSound;
	private AudioSource audioS;

	private CRaycaster rc;

	// Use this for initialization
	void Start () {
	
		audioS = GetComponent<AudioSource> ();
		rc = GetComponent<CRaycaster> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (mapOpen) {

			switch (displayedFloor) {

			case 0:
				mapUG.SetActive (true);
				mapEG.SetActive (false);
				mapOG.SetActive (false);

				UGText.color = Color.cyan;
				EGText.color = Color.white;
				OGText.color = Color.white;
				break;

			case 1:
				mapEG.SetActive (true);
				mapUG.SetActive (false);
				mapOG.SetActive (false);

				EGText.color = Color.cyan;
				UGText.color = Color.white;
				OGText.color = Color.white;
				break;

			case 2:
				mapOG.SetActive (true);
				mapUG.SetActive (false);
				mapEG.SetActive (false);

				OGText.color = Color.cyan;
				UGText.color = Color.white;
				EGText.color = Color.white;

				break;
			}
		}

		if (Input.GetKeyDown (KeyCode.M) && !rc.activeObject && !rc.activeNPC) {

			audioS.PlayOneShot (mapSound);

			if (!mapOpen) 
			{
				map.SetActive (true);
				mapOpen = true;
			} 
			else 
			{
				map.SetActive (false);
				mapOpen = false;
			}
		}

		if (Input.GetKeyDown (KeyCode.Q) && mapOpen) 
		{
			if (mapOpen) {

				if (displayedFloor > 0) {
					audioS.PlayOneShot (mapSound);
					displayedFloor -= 1;
				}
			}
		}

		if (Input.GetKeyDown (KeyCode.E) && mapOpen) 
		{
			if (mapOpen) {
				
				if (displayedFloor < 2) {
					audioS.PlayOneShot (mapSound);
					displayedFloor += 1;
				}
			}
		}
	}
}
