﻿using UnityEngine;
using System.Collections;

public class CNPCTrigger : MonoBehaviour {

	public int progressNumber;

	public GameObject NPC;
	private CDialoges dg;

	public GameObject dialoges;

	// Use this for initialization
	void Start () {

		dg = NPC.GetComponentInChildren<CDialoges> ();
	}

	void OnTriggerEnter(Collider other){

		if (other.tag == "NPC") 
		{
			dg.dialogeProgress = progressNumber;
			dialoges.SetActive (true);
			Destroy (this.gameObject);
		}
	}
}
