﻿using UnityEngine;
using System.Collections;

public class CDoor : MonoBehaviour {

	public int keyID;
	public bool locked = true;
	private Rigidbody rb;

	// Use this for initialization
	void Start () {
	
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {


		if (!locked)
			rb.isKinematic = false;
		else
			rb.isKinematic = true;

	}
}
