﻿using UnityEngine;
using System.Collections;

public class CFlashlight : MonoBehaviour {

	public GameObject flashlight;
	private Light light;
	public bool gotFlashlight = false;
	public bool gotBatteries = false;
	public bool flashlightOn = false;

	private float timer;
	public float min = 1.0f;
	public float max = 3.0f;
	public bool smooth = true;
	public float flickerTime = 0.5f;

	private CMap map;
	private CRaycaster rc;

	// Use this for initialization
	void Start () {
	
		timer = flickerTime;
		map = GetComponent<CMap> ();
		rc = GetComponent<CRaycaster> ();
		light = flashlight.GetComponent<Light> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.F) && gotFlashlight && gotBatteries && !map.mapOpen && !rc.activeObject && !rc.activeNPC) {
			flashlightOn = !flashlightOn;

			if (flashlightOn)
				flashlight.SetActive (true);
			else
				flashlight.SetActive (false);
		}

		timer -= Time.deltaTime;

		if (timer < 0) 
		{
			timer = flickerTime;

			if (flashlightOn) 
			{
				if (smooth) 
				{
					float noise = Mathf.PerlinNoise (1.0f, Time.time);
					light.intensity = Mathf.Lerp (min, max, noise);
				} 
				else					
					light.intensity = Random.Range (min, max);
			}
		}
	}
}
