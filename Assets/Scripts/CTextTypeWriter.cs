﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CTextTypeWriter : MonoBehaviour {

	private Text text;
	private string story;

	private Text[] nextText;

	public float speed = 0.001f;

	public CRaycaster rc;

	private CDialoges dg;
	private CMonologe mg;
	public GameObject dialoges;
	public GameObject monologes;


	void OnEnable()
	{
		if (rc.activeNPC) 
		{
			dg = rc.activeNPC.GetComponentInChildren<CDialoges> ();
			nextText = dg.Dialoges;

			text = GetComponentInChildren<Text> ();
			story = nextText [dg.dialogeProgress].text;
			text.text = "";

			StartCoroutine ("PlayText");

			if (dg.dialogeProgress < nextText.Length - 1)
				dg.dialogeProgress++;
		}
		else 
		{
			//NPC from far away
			/*dg = dialoges.GetComponentInChildren<CDialoges> ();
			nextText = dg.Dialoges;

			text = GetComponentInChildren<Text> ();
			story = nextText [dg.dialogeProgress].text;
			text.text = "";*/

			//Dialog
			mg = monologes.GetComponentInChildren<CMonologe> ();
			nextText = mg.monologes;

			text = GetComponentInChildren<Text> ();
			story = nextText [mg.monologeProgress].text;
			text.text = "";

			StartCoroutine ("PlayText");
		}
	}

	IEnumerator PlayText()
	{
		foreach (char c in story) 
		{
			text.text += c;
			yield return new WaitForSeconds (speed);
		}
	}

}