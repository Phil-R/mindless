﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;

public class CInventory : MonoBehaviour {

	private CRaycaster rc;
	private DepthOfFieldDeprecated dof;
	private FirstPersonController fpc;
	private CRotateObject ro;
	private CMap map;

	private int position;

	// Use this for initialization
	void Start () {
	
		rc = GetComponent<CRaycaster> ();
		dof = GetComponent<DepthOfFieldDeprecated> ();
		fpc = GetComponentInParent<FirstPersonController> ();
		ro = GetComponentInParent<CRotateObject> ();
		map = GetComponent<CMap> ();
	}
	
	// Update is called once per frame
	void Update () {	

		if (!rc.activeObject && !map.mapOpen) 
		{
			if (Input.GetKeyDown (KeyCode.Alpha1) || Input.GetKeyDown (KeyCode.Alpha2) || Input.GetKeyDown (KeyCode.Alpha3) || Input.GetKeyDown (KeyCode.Alpha4) || Input.GetKeyDown (KeyCode.Alpha5)
			   || Input.GetKeyDown (KeyCode.Alpha6) || Input.GetKeyDown (KeyCode.Alpha7) || Input.GetKeyDown (KeyCode.Alpha8) || Input.GetKeyDown (KeyCode.Alpha9) || Input.GetKeyDown (KeyCode.Alpha0)) {
				if (Input.inputString != "0") 
				{
					position = int.Parse (Input.inputString);
					position -= 1;
				} 
				else if (Input.inputString == "0") 
				{
					position = int.Parse (Input.inputString);
					position = 9;
				}

				if (rc.itemInventory [position] != null) 
				{
					rc.activeObject = rc.itemInventory [position];

					dof.enabled = true;
					fpc.enabled = false;
					ro.enabled = true;

					rc.keyInventory [position] = 0;
					rc.inventoryPictures [position].sprite = rc.emptyPicture;
					rc.itemInventory [position].transform.position = rc.viewPos.position;
					rc.itemInventory [position].SetActive (true);
					rc.itemInventory [position] = null;
				}
			}
		}
	}
}
