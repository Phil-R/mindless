﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class CnpcStartsDialoge : MonoBehaviour {

	public GameObject dialoge;
	private CDialoges dg;

	private AICharacterControl aicc;
	public GameObject target;

	public GameObject player;
	private CRaycaster rc;

	void Start()
	{
		rc = player.GetComponentInChildren<CRaycaster> ();
		aicc = GetComponentInParent<AICharacterControl> ();
		dg = GetComponentInChildren<CDialoges> ();
	}

	void Update()
	{
		if (rc.activeNPC) 
		{
			switch (dg.dialogeProgress)
			{
			case 2:
				aicc.target = target.transform;
				break;
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") {

			rc.activeNPC = transform.parent.gameObject;
			dialoge.SetActive (true);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Player") 
		{
			dialoge.SetActive (false);
		}
	}
}
