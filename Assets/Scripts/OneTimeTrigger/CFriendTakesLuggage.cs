﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class CFriendTakesLuggage : MonoBehaviour {

	public GameObject[] Luggage;
	public GameObject friend;
	private AICharacterControl aicc;
	public Transform nextTarget;

	void Start()
	{
		aicc = friend.GetComponent<AICharacterControl> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "NPC") {

			for (int i = 0; i < Luggage.Length; i++)
				Destroy (Luggage [i]);

			aicc.target = nextTarget;
			Destroy (this.gameObject);
		}
	}
}
