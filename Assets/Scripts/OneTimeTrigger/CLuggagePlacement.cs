﻿using UnityEngine;
using System.Collections;

public class CLuggagePlacement : MonoBehaviour {

	public bool luggageCanBePlaced = false;
	public Transform target;
	public Sprite emptyPicture;
	public GameObject player;
	private CRaycaster rc;

	public GameObject key;

	void Start()
	{
		rc = player.GetComponentInChildren<CRaycaster> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			for (int i = 0; i < rc.itemInventory.Length; i++) 
			{
				if (rc.itemInventory [i] != null) 
				{					
					if (rc.itemInventory [i].name == "Luggage") 
					{						
						rc.itemInventory [i].transform.position = target.position;
						rc.itemInventory [i].transform.rotation = target.rotation;
						Collider col = rc.itemInventory [i].GetComponent<Collider> ();
						col.enabled = false;
						Collider keyCol = key.GetComponent<Collider> ();
						keyCol.enabled = true;
						rc.itemInventory [i].SetActive (true);
						rc.inventoryPictures [i].sprite = emptyPicture;
						rc.keyInventory [i] = 0;
						rc.itemInventory [i] = null;
						Destroy (this.gameObject);
					}
				}
			}
		}
	}
}
