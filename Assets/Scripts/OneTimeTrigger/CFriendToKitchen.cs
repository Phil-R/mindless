﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.UI;

public class CFriendToKitchen : MonoBehaviour {

	public AICharacterControl aicc;
	public Transform target;

	public GameObject player;
	private CRaycaster rc;
	public Sprite empty;

	public GameObject nextActivity;
	public GameObject nextTarget;

	void Start()
	{
		rc = player.GetComponentInChildren<CRaycaster> ();
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			for (int i = 0; i < rc.itemInventory.Length; i++) {

				if (rc.itemInventory[i] != null && rc.itemInventory [i].name == "Bread") {
					rc.itemInventory [i] = null;
					rc.inventoryPictures [i].sprite = empty;
					rc.keyInventory [i] = 0;
				}

				if (rc.itemInventory[i] != null && rc.itemInventory [i].name == "Ham") {
					rc.itemInventory [i] = null;
					rc.inventoryPictures [i].sprite = empty;
					rc.keyInventory [i] = 0;
				}

				if (rc.itemInventory[i] != null && rc.itemInventory [i].name == "Cheese") {
					rc.itemInventory [i] = null;
					rc.inventoryPictures [i].sprite = empty;
					rc.keyInventory [i] = 0;
				}

				if (rc.itemInventory[i] != null && rc.itemInventory [i].name == "Salat") {
					rc.itemInventory [i] = null;
					rc.inventoryPictures [i].sprite = empty;
					rc.keyInventory [i] = 0;
				}
			}

			aicc.target = target;
			Collider col = nextTarget.GetComponent<Collider> ();
			col.enabled = true;
			col = nextActivity.GetComponent<Collider> ();
			col.enabled = true;
			Destroy (this.gameObject);
		}
	}
}
