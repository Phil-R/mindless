﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class CFriendSendsYouToBed : MonoBehaviour {

	public GameObject friend;
	public GameObject player;

	void Start()
	{

	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Player") 
		{
			AICharacterControl aicc = friend.GetComponent<AICharacterControl> ();
			aicc.target = player.transform;
			Destroy (this.gameObject);
		}
	}
}
