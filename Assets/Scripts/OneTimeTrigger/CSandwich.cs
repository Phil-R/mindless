﻿using UnityEngine;
using System.Collections;

public class CSandwich : MonoBehaviour {

	public bool hasBread = false;
	public bool hasHam = false;
	public bool hasCheese = false;
	public bool hasSalat = false;

	public GameObject next;

	private CRaycaster rc;

	// Use this for initialization
	void Start () {
	
		rc = GetComponent<CRaycaster> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (hasBread && hasHam && hasCheese && hasSalat) {

			next.SetActive (true);
			Destroy (this);
		}
	}
}
