﻿using UnityEngine;
using System.Collections;

public class CDoSomething : MonoBehaviour {

	public GameObject bed;

	public void UnlockBed()
	{
		Collider col = bed.GetComponent<Collider> ();
		col.enabled = true;
	}
}
