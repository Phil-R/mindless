﻿using UnityEngine;
using System.Collections;

public class CSafeWheels : MonoBehaviour {

	public AudioClip clickSound;
	private AudioSource audioS;

	public Vector3[] Numbers;
	public int displayedNumber = 2;
	public bool isWheelActive = false;

	public GameObject player;
	private CRaycaster rc;

	// Use this for initialization
	void Start () {
	
		audioS = player.GetComponentInChildren<AudioSource> ();
		rc = player.GetComponentInChildren<CRaycaster> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (rc.activeObject && rc.activeObject.tag == "Riddle")
		{
			if (Input.GetAxis ("Mouse ScrollWheel") < 0) 
			{
				if (displayedNumber < 9) 
				{
					displayedNumber++;
				} 
				else
					displayedNumber = 0;

				StartCoroutine ("MovableWheel");
				audioS.PlayOneShot (clickSound);
			}

			if (Input.GetAxis ("Mouse ScrollWheel") > 0) 
			{
				if (displayedNumber > 0) 
				{
					displayedNumber--;
				} 
				else
					displayedNumber = 9;

				StartCoroutine ("MovableWheel");
				audioS.PlayOneShot (clickSound);
			}
		}
	}

	void OnEnable() {

		transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y - 0.00025f, transform.localPosition.z);
	}

	void OnDisable() {

		transform.localPosition = new Vector3 (transform.localPosition.x, transform.localPosition.y + 0.00025f, transform.localPosition.z);
	}

	IEnumerator MovableWheel()
	{
		for (float f = 0.0f; f < 1.0f; f += 0.1f) {
			
			transform.localRotation = Quaternion.Lerp (transform.localRotation, Quaternion.Euler (Numbers [displayedNumber]), f);
			yield return null;
		}
	}
}
