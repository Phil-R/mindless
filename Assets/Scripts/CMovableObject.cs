﻿using UnityEngine;
using System.Collections;

public class CMovableObject : MonoBehaviour {

	public Vector3 originPos;
	public Vector3 targetPos;

	public Vector3 originRot;
	public Vector3 targetRot;

	public bool isMoving = false;
	public bool atTarget = false;

	public AudioClip sound;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
