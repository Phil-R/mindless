﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.FirstPerson;

public class CSafe : MonoBehaviour {

	public GameObject riddleCam;
	public AudioClip safeUnlock;
	public int[] combination;
	public bool isOpen = false;

	public GameObject[] wheels;

	public int activeWheel = 0;

	public GameObject player;
	private FirstPersonController fpc;
	private CRaycaster rc;
	private Camera cam;
	private AudioListener al;
	private AudioSource audioS;
	public AudioClip clickSound;

	private CSafeWheels sw0;
	private CSafeWheels sw1;
	private CSafeWheels sw2;

	private Collider col;
	private Collider coverCol;
	public GameObject cover;

	// Use this for initialization
	void Start () {
	
		rc = player.GetComponentInChildren<CRaycaster> ();
		fpc = player.GetComponent<FirstPersonController> ();
		cam = player.GetComponentInChildren<Camera> ();
		al = player.GetComponentInChildren<AudioListener> ();
		audioS = player.GetComponentInChildren<AudioSource> ();

		col = GetComponentInParent<Collider> ();
		coverCol = cover.GetComponent<Collider> ();

		sw0 = wheels [0].GetComponent<CSafeWheels> ();
		sw1 = wheels [1].GetComponent<CSafeWheels> ();
		sw2 = wheels [2].GetComponent<CSafeWheels> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (rc.activeObject && !isOpen) {
			if ((Input.GetKeyDown (KeyCode.A) || Input.GetKeyDown (KeyCode.D)) && rc.activeObject.tag == "Riddle") {

				if (Input.GetKeyDown (KeyCode.A)) {

					audioS.PlayOneShot (clickSound);

					if (activeWheel > 0)
						activeWheel--;
				}

				if (Input.GetKeyDown (KeyCode.D)) {

					audioS.PlayOneShot (clickSound);

					if (activeWheel < 2)
						activeWheel++;
				}

				switch (activeWheel) {
				case 0:
				
					sw0.enabled = true;
					sw1.enabled = false;
					sw2.enabled = false;
					break;

				case 1:
				
					sw0.enabled = false;
					sw1.enabled = true;
					sw2.enabled = false;
					break;

				case 2:

					sw0.enabled = false;
					sw1.enabled = false;
					sw2.enabled = true;
					break;
				}
			}

			if (sw0.displayedNumber == combination [0] && sw1.displayedNumber == combination [1] && sw2.displayedNumber == combination [2] && !isOpen) 
			{
				isOpen = true;
				col.enabled = false;
				coverCol.enabled = true;

				rc.activeObject = null;
				riddleCam.SetActive (false);
				fpc.enabled = true;
				cam.enabled = true;
				al.enabled = true;

				audioS.PlayOneShot (safeUnlock);
			}
		}
	}
}
