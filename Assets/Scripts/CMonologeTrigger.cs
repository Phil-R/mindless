﻿using UnityEngine;
using System.Collections;

public class CMonologeTrigger : MonoBehaviour {

	public int progressNumber;

	public GameObject player;
	private CMonologe mg;

	public GameObject dialoges;

	// Use this for initialization
	void Start () {
	
		mg = player.GetComponentInChildren<CMonologe> ();
	}

	void OnTriggerEnter(Collider other){

		if (other.tag == "Player") 
		{
			mg.monologeProgress = progressNumber;
			dialoges.SetActive (true);
			Destroy (this.gameObject);
		}
	}
}
