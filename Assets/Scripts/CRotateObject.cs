﻿using UnityEngine;
using System.Collections;

public class CRotateObject : MonoBehaviour {

	private CRaycaster rc;
	private GameObject activeObject;
	private Camera cam;

	public float speed;
	public float zoom = 0.0f;

	public GameObject viewPos;

	// Use this for initialization
	void Start () {
	
		rc = transform.GetComponentInChildren<CRaycaster> ();
		cam = GetComponentInChildren<Camera> ();
	}
	
	// Update is called once per frame
	void Update () {

		activeObject = rc.activeObject;
		activeObject.transform.Rotate (0, Input.GetAxis ("Mouse X") * 3, 0, Space.World);
		activeObject.transform.Rotate (0, Input.GetAxis ("Mouse Y") * 3, 0, Space.Self);

		if (Input.GetAxis ("Mouse ScrollWheel") > 0) {
			if (zoom < 2.9f) {
				zoom += 0.2f;
				cam.fieldOfView -= 3.0f;
			}
		}

		if (Input.GetAxis ("Mouse ScrollWheel") < 0) {
			if (zoom > 0.0f) {
				zoom -= 0.2f;
				cam.fieldOfView += 3.0f;
			}
		}	
	}

	void OnDisable()
	{
		zoom = 0.0f;
		cam.fieldOfView = 60;
	}


}
