﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.FirstPerson;
using UnityStandardAssets.Characters.ThirdPerson;

public class CRaycaster : MonoBehaviour {

	//Raycast
	public float rayDistance = 5.0f;
	private RaycastHit hit;
	public Text itemName;

	//The items you interact with
	public GameObject activeObject;
	public GameObject activeObjectParent;
	public GameObject activeNPC;
	public GameObject dialoge;
	public GameObject sky;

	//Night
	public GameObject creepySounds;
	public GameObject friendTrigger;
	public GameObject friend;
	public Transform friendTargetNight;
	private AICharacterControl aicc;

	private Camera cam;
	private AudioListener al;

	public Transform viewPos = null;
	private Vector3 itemPos;
	private Quaternion itemRot;

	//Components needed
	private AudioSource audioS;
	private CItemSpecs itemSpecs;
	private DepthOfFieldDeprecated dof;
	private FirstPersonController fpc;
	private CMovableObject mo;
	private CRotateObject ro;
	private CDoor cdoor;
	private CLightSwitch ls;
	private CMap map;
	private CSafe safe;
	private CFadeBlack fb;
	private CSandwich sw;
	private CFlashlight fl;

	//Inventory
	public int[] keyInventory;
	public GameObject[] itemInventory;
	public Image[] inventoryPictures;
	public Sprite emptyPicture;
	public AudioClip sPickup;
	public AudioClip sDoorLocked;
	public AudioClip sDoorUnlock;
	public AudioClip sDoorMove;

	// Use this for initialization
	void Start () {

		cam = GetComponent<Camera> ();
		al = GetComponent<AudioListener> ();
		audioS = GetComponent<AudioSource> ();
		dof = GetComponent<DepthOfFieldDeprecated> ();
		fpc = GetComponentInParent<FirstPersonController> ();
		ro = GetComponentInParent<CRotateObject> ();
		map = GetComponent<CMap> ();
		fb = GetComponent<CFadeBlack> ();
		sw = GetComponent<CSandwich> ();
		fl = GetComponent<CFlashlight> ();
		aicc = friend.GetComponent <AICharacterControl> ();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.E) && dialoge.activeInHierarchy) 
		{
			dialoge.SetActive (false);
			activeNPC = null;
		}
		
		Debug.DrawRay (transform.position, transform.forward * rayDistance, Color.green);

		if (Physics.Raycast (transform.position, transform.forward, out hit, rayDistance) && !map.mapOpen) 
		{
				
			//Interact with Objects or NPCs
			if (hit.transform.tag == "Item" || hit.transform.tag == "Pickup" || hit.transform.tag == "NPC" || hit.transform.tag == "Door" || 
				hit.transform.tag == "Moveable" || hit.transform.tag == "Switch" || hit.transform.tag == "Riddle" || hit.transform.tag == "Bed")
			{
				itemName.text = hit.transform.name;

				if (Input.GetKeyDown (KeyCode.E))
				{

					if (hit.transform.tag == "Item" || hit.transform.tag == "Pickup") {
						
						if (!activeObject) {
							
							activeObject = hit.transform.gameObject;

							if (activeObject.transform.parent) {
								activeObjectParent = activeObject.transform.parent.gameObject;
								activeObject.transform.SetParent (null, true);
							}

							itemPos = activeObject.transform.position;
							itemRot = activeObject.transform.rotation;
							activeObject.transform.position = viewPos.position;

							dof.enabled = true;
							fpc.enabled = false;
							ro.enabled = true;
						} 

						//Pickups
						else {
							if (hit.transform.tag == "Pickup") 
							{
								itemSpecs = hit.transform.GetComponent<CItemSpecs> ();

								if (itemSpecs.keyID == 0) 
								{
									CDoSomething tpub = hit.transform.GetComponent<CDoSomething> ();
									tpub.UnlockBed ();
									Destroy (hit.transform.gameObject);
									audioS.PlayOneShot (sPickup);
								} 

								else 
								{
									for (int i = 0; i < keyInventory.Length; i++) {
										if (keyInventory [i] == 0) {
											itemInventory [i] = activeObject;
											keyInventory [i] = itemSpecs.keyID;
											inventoryPictures [i].sprite = itemSpecs.itemPicture;
											break;
										}
									}
									
									audioS.PlayOneShot (sPickup);
									activeObject.SetActive (false);
									activeObject = null;

									if (hit.transform.name == "Bread")
										sw.hasBread = true;

									if (hit.transform.name == "Ham")
										sw.hasHam = true;

									if (hit.transform.name == "Cheese")
										sw.hasCheese = true;

									if (hit.transform.name == "Salat")
										sw.hasSalat = true;

									if (hit.transform.name == "Battery")
										fl.gotBatteries = true;

									if (hit.transform.name == "Flashlight")
										fl.gotFlashlight = true;
								}

							} 

							else 
							{
								activeObject.transform.position = itemPos;
								activeObject.transform.rotation = itemRot;

								if (activeObjectParent) {
									activeObject.transform.SetParent (activeObjectParent.transform, true);
									activeObjectParent = null;
								}

								activeObject = null;
							}

							ro.enabled = false;
							fpc.enabled = true;
							dof.enabled = false;
						}
					}

					//NPCs
					else if (hit.transform.tag == "NPC") {
						if (!activeNPC) {
							activeNPC = hit.transform.gameObject;
						} else
							activeNPC = null;

						if (dialoge.activeInHierarchy)
							dialoge.SetActive (false);
						else
							dialoge.SetActive (true);
					}

					//Doors
					else if (hit.transform.tag == "Door") {
						cdoor = hit.transform.GetComponent<CDoor> ();

						for (int i = 0; i < keyInventory.Length; i++) {
							if (keyInventory [i] != 0) {
								if (cdoor.keyID == keyInventory [i]) {
									audioS.PlayOneShot (sDoorUnlock);
									cdoor.locked = false;
									keyInventory [i] = 0;
									itemInventory [i] = null;
									inventoryPictures [i].sprite = emptyPicture;
									break;
								}								
							}
						}

						if (cdoor.locked == true)
							audioS.PlayOneShot (sDoorLocked);
						else
							audioS.PlayOneShot (sDoorMove);

						Rigidbody rb = hit.transform.GetComponent<Rigidbody> ();
						rb.AddForce (transform.forward * 150);
					} 

					//Moveables
					else if (hit.transform.tag == "Moveable") {						
						StartCoroutine ("MovingObject");
					}

					//Switches
					else if (hit.transform.tag == "Switch") 
					{
						if(hit.transform.GetComponent<CMovableObject>() != null)
							StartCoroutine ("MovingObject");

						ls = hit.transform.GetComponent<CLightSwitch> ();

						audioS.PlayOneShot (ls.switchSound);
						ls.lightsOn = !ls.lightsOn;

						if (ls.lightsOn) {
							for (int i = 0; i < ls.lights.Length; i++) {
								ls.lights [i].SetActive (true);
							}
						} else {
							for (int i = 0; i < ls.lights.Length; i++) {
								ls.lights [i].SetActive (false);
							}
						}
					}

					//Riddles
					else if (hit.transform.tag == "Riddle") {

						if (!activeObject) {
							cam.enabled = false;
							al.enabled = false;
							activeObject = hit.transform.gameObject;
							safe = hit.transform.GetComponentInChildren<CSafe> ();
							safe.riddleCam.SetActive (true);
							fpc.enabled = false;
						} else {
							activeObject = null;
							safe.riddleCam.SetActive (false);
							fpc.enabled = true;
							cam.enabled = true;
							al.enabled = true;
						}
					} 

					//Bed
					else if (hit.transform.tag == "Bed") {
						fpc.enabled = false;
						fb.fadeToBlack = true;
						aicc.target = friendTargetNight;
						friend.transform.position = friendTargetNight.position;
						Invoke ("FadeFromBlack", 5.0f);
					}
				}
			}
			else
				itemName.text = "";
		}
		else
			itemName.text = "";
	}

	IEnumerator MovingObject()
	{
		mo = hit.transform.GetComponent<CMovableObject> ();
		audioS.PlayOneShot (mo.sound, 0.5f);

		if (!mo.isMoving) {
			if (!mo.atTarget) {
				for (float f = 0.0f; f < 1.0f; f += 0.1f) 
				{
					mo.isMoving = true;
					mo.transform.localPosition = Vector3.Lerp (mo.transform.localPosition, mo.targetPos, f);
					mo.transform.localRotation = Quaternion.Lerp (mo.transform.localRotation, Quaternion.Euler(mo.targetRot), f);
					yield return null;
				}

				mo.isMoving = false;
				mo.atTarget = true;
			} 
			else 
			{
				for (float f = 0.0f; f < 1.0f; f += 0.1f) {
					mo.isMoving = true;
					mo.transform.localPosition = Vector3.Lerp (mo.transform.localPosition, mo.originPos, f);
					mo.transform.localRotation = Quaternion.Lerp (mo.transform.localRotation, Quaternion.Euler(mo.originRot), f);
					yield return null;
				}

				mo.isMoving = false;
				mo.atTarget = false;
			}
		}
	}

	void FadeFromBlack()
	{
		creepySounds.SetActive (true);
		sky.transform.rotation = Quaternion.Euler (220, 90, 0);
		fb.fadeToBlack = false;
		fpc.enabled = true;
		friendTrigger.SetActive (true);
	}
}
